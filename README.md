# Challenge Chapter ke-6 : Binar Car Management RESTful API

This project challenge chapter 6 from Binar Academy with mentor [Imam Hermawan](https://gitlab.com/ImamTaufiqHermawan)

## Getting Started

mulainya menginspeksi file [`app/index.js`](./app/index.js), dan lihatlah salah satu contoh `controller` yang ada di [`app/controllers/mainController.js`](./app/controllers/mainController.js)

Lalu untuk menjalankan development server, kalian tinggal jalanin salah satu script di package.json, yang namanya `develop`.

## Module node.js

```
- npm install
```

## Run Server

> $ npm run start

## Database Management

Di repository ini sudah terdapat beberapa perintah script yang dapat digunakan dalam memanage database, sebagai berikut:

- `npm db:create` digunakan untuk membuat database
- `npm db:drop` digunakan untuk menghapus database
- `npm db:migrate` digunakan untuk menjalankan database migration
- `npm db:seed` digunakan untuk melakukan seeding
- `npm db:rollback` digunakan untuk membatalkan migrasi terakhir

## Cars CRUD
- `async list()` digunakan untuk menampilkan semua data mobil (cars) di database
- `async create()` digunakan untuk membuat data mobil (cars) yang baru agar dapat masuk ke database
- `async update()` digunakan untuk memperbarui data mobil (cars) di database menjadi data mobil yang baru
- `async show()` digunakan untuk menampilkan data mobil (cars) di database sesuai ID
- `async destroy()` digunakan untuk menghapus data mobil (cars) di database

## Users CRUD
- `async register()` digunakan untuk register (mendaftar) data users agar masuk kedalam database
- `async login()` digunakan untuk login (masuk) menggunakan data users yang sudah terdaftar
- `async authorize()` digunakan untuk otorisasi data users menggunakan token sesuai login
- `async isAdminOrSuperAdmin()` digunakan untuk otorisasi roles users apakah dia admin atau superadmin
- `async isSuperAdmin()` digunakan untuk otorisasi roles users apakah dia superAdmin
- `async getUsers()` digunakan untuk menampilkan semua data users di database
- `async create()` digunakan untuk membuat data users yang baru agar dapat masuk ke database
- `async update()` digunakan untuk memperbarui data users di database menjadi data mobil yang baru
- `async show()` digunakan untuk menampilkan data users di database sesuai ID
- `async destroy()` digunakan untuk menghapus data users di database

## Data User

### SuperAdmin

```
{
  "email": "superadmin@gmail.com",
  "password": "superadmin17"
}
```

### Admin

```
{
  "email": "admin@gmail.com",
  "password": "admin17"
}
```

### Member

```
{
  "email": "deka.mersandi@gmail.com",
  "password": "dekamersandi17"
}
```
