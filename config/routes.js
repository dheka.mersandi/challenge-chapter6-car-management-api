const express = require("express");
const res = require("express/lib/response");
const controllers = require("../app/controllers");
const middewares = require("../app/middlewares");
const apiRouter = express.Router();

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../data/swagger.json");

/**
 * TODO: Implement your own API
 *       implementations
 */

/**ROUTING */

// Login using SuperAdmin, Admin, or Member
apiRouter.post("/api/v1/login", controllers.api.v1.userController.login);

// for add admin by superadmin
apiRouter.put(
  "/api/v1/users/:id",
  controllers.api.v1.userController.authorize,
  middewares.checkCredential(["superadmin"]),
  controllers.api.v1.userController.update
);

// Register For Member
apiRouter.post("/api/v1/register", controllers.api.v1.userController.register);

// whoami
apiRouter.get("/api/v1/whoami", controllers.api.v1.userController.authorize, controllers.api.v1.userController.whoAmI);

// Create Car by SuperAdmin or Admin 
apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.userController.authorize,
  middewares.checkCredential(["superadmin", "admin"]),
  controllers.api.v1.carController.create
);

// Read one car by SuperAdmin or Admin
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  middewares.checkCredential(["superadmin", "admin"]),
  controllers.api.v1.carController.show
);

// Update car by SuperAdmin or Admin
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  middewares.checkCredential(["superadmin", "admin"]),
  controllers.api.v1.carController.update
);

// Delete car by SuperAdmin or Admin
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  middewares.checkCredential(["superadmin", "admin"]),
  controllers.api.v1.carController.makeCarDeleted
);

// Read all car which is not deleted
apiRouter.get("/api/v1/cars", controllers.api.v1.carController.list);

// Read all deleted car by SuperAdmin or Admin
apiRouter.get(
  "/api/v1/history",
  controllers.api.v1.userController.authorize,
  middewares.checkCredential(["superadmin", "admin"]),
  controllers.api.v1.carController.listArgs({
    where: { isDeleted: true },
  })
);

// open api document
apiRouter.use("/api-docs", swaggerUi.serve);
apiRouter.get("/api-docs", swaggerUi.setup(swaggerDocument));

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error("The Industrial Revolution and its consequences have been a disaster for the human race.");
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
